package info.hccis.canes.entity;

import java.io.Serializable;

import info.hccis.canes.util.Util;

/**
 * User class
 * @author mblanchard9255
 * @since 20200207
 * @modified BJM 20200207
 */
public class UserAccess implements Serializable {

//    public static final String BASE_API = Util.BASE_SERVER+"court/rest/UserAccessService/";
//    public static final String BASE_API = Util.BASE_SERVER+"court/rest/UserAccessService/";
//    public static final String BASE_API = Util.BASE_SERVER+"court/rest/UserAccessService/";

    //Using Fred's server for the canes authentication.  The canes web app doesn't have user access login functionality
    public static final String BASE_API = "http://hccis.info:8080/"+"court/rest/UserAccessService/";

    private static final long serialVersionUID = 1L;
    private Integer userId;
    private String username;
    private String password;
    private String lastName;
    private String firstName;
    private int userTypeCode;
    private String additional1;
    private String additional2;
    private String createdDateTime;

    public UserAccess() {
    }

    public UserAccess(Integer userId) {
        this.userId = userId;
    }

    public UserAccess(Integer userId, String username, String password, String lastName, String firstName, int userTypeCode) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;
        this.userTypeCode = userTypeCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(int userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    public String getAdditional1() {
        return additional1;
    }

    public void setAdditional1(String additional1) {
        this.additional1 = additional1;
    }

    public String getAdditional2() {
        return additional2;
    }

    public void setAdditional2(String additional2) {
        this.additional2 = additional2;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    @Override
    public String toString() {
        return "UserAccess{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", userTypeCode=" + userTypeCode +
                ", additional1='" + additional1 + '\'' +
                ", additional2='" + additional2 + '\'' +
                ", createdDateTime='" + createdDateTime + '\'' +
                '}';
    }
}
