package info.hccis.camper.repositories;

import info.hccis.camper.jpa.entity.CamperType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperTypeRepository extends CrudRepository<CamperType, Integer> {
}