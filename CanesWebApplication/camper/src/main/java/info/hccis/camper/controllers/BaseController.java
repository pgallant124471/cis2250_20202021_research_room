package info.hccis.camper.controllers;

import info.hccis.camper.bo.CamperTypeBO;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperTypeRepository;
import info.hccis.camper.util.CisUtility;
import info.hccis.camper.util.UtilityRest;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    private final CamperTypeRepository camperTypeRepository;

    public BaseController(CamperTypeRepository ctr) {
        camperTypeRepository = ctr;
    }

    @RequestMapping("/")
    public String home(HttpSession session) {

        String urlString = "http://localhost:8081/api/CamperService/campers/123";
        try {
            UtilityRest.getJsonFromRest(urlString);
        } catch (Exception e) {
            System.out.println("There was an error accessing that rest service.");
        }

        //BJM 20200602 Issue#1 Set the current date in the session
        String currentDate = CisUtility.getCurrentDate("yyyy-MM-dd");
        session.setAttribute("currentDate", currentDate);

        //Load the camper types into the session
//        CamperTypeBO camperTypeBO = new CamperTypeBO();
//        ArrayList<CamperType> camperTypes = new ArrayList();
//        camperTypes = camperTypeBO.load();
        ArrayList<CamperType> camperTypes = (ArrayList<CamperType>) camperTypeRepository.findAll();
        session.setAttribute("camperTypes", camperTypes);
        session.setAttribute("camperTypesArray",camperTypes.toArray());
        System.out.println("BJM, loaded " + camperTypes.size() + " camper types");

        HashMap<Integer, CamperType> camperTypesMap = new HashMap();
        camperTypesMap.clear();
        for (CamperType current : camperTypes) {
            camperTypesMap.put(current.getId(), current);
        }
        CamperTypeBO.setCamperTypesMap(camperTypesMap);
        session.setAttribute("camperTypesMap", camperTypesMap);

        
        return "index";
    }

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
